#!/bin/bash

file=$1

convert -strip -interlace Plane -sampling-factor 4:2:0 -quality 85% $file tmp.jpg

mv tmp.jpg $file

exit 0
