#!/bin/bash
folder=$1

if [ -d $folder ]; then
    for filename in $folder/*; do
        if [ -f $filename ] ; then
            # length=$(soxi -d $filename)
            sox $filename "tmp.wav" fade 0.01 -0 0.01 
            mv "tmp.wav" $filename
            echo $filename
        fi
    done
fi
