#!/bin/bash

if [[ $# -ne 3 ]]; then
    echo "Please provide invideo, inaudio and outvideo args" 
else
    invideo = $1
    inaudio = $2
    outvideo = $3
    # ffmpeg -i $invideo -i $inaudio -c:v copy -c:a aac -map 0:v:0 -map 1:a:0 $outvideo
    echo $invideo $outvideo $inaudio
fi
