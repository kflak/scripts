#!/bin/bash

dir=$HOME/screenshots

if [[ ! -d $dir ]]; then
    mkdir $dir
fi

slurp | grim -g - $dir/$(date +%F_%T).png
