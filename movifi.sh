#/bin/bash

infile=$1
basename=$(basename $infile .m4v)
newname=$basename.mov
ffmpeg -i $infile -vcodec copy $newname
