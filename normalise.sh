#!/bin/bash
folder=$1
# extension='_mono.wav'

if [ -d $folder ]; then
    for filename in $folder/*; do
        if [ -f $filename ] ; then
            sox --norm=-12 $filename "tmp.wav"
            mv "tmp.wav" $filename
            echo $filename
        fi
    done
fi
