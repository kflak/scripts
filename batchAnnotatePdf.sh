#!/bin/bash

dest=done
if [[ ! -d ./done ]]; then
    mkdir done
fi

for file in *.pdf; do
    xournalpp "$file" && mv "$file" $dest
done
