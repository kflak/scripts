#!/bin/bash

if [ $( cat /sys/class/backlight/gmux_backlight/brightness ) -lt 1024 ]
then 
    gmux_backlight +20
fi
