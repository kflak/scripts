#!/bin/bash

dir=$HOME/screenshots

if [[ ! -d $dir ]]; then
    mkdir $dir
fi

grim $dir/$(date +%F_%T).png
