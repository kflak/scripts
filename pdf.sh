#!/bin/bash

if [ -f $1 ] && [ $# -gt 0 ]; then
    infile=$1
    if [[ $# -gt 1 ]]; then
        lang=$2
    else
        lang=en-US
    fi
    basename=$(echo "$infile" | cut -f 1 -d '.')
    outfile=$basename.pdf
    pandoc -o $outfile $infile --template eisvogel --listings -V lang=$lang 
    zathura $outfile &
    exit 0
else
    echo "Usage: pdf.sh infile language"
    exit 1
fi
