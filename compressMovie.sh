#!/bin/zsh

file=$1
if [[ -f $file ]]; then
    filename="${file%.(mov|mp4|m4v)}"
    ffmpeg -i '$file' -vcodec libx265 -crf 28 -acodec mp2 '$filename.mp4'
fi
