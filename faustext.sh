#!/bin/sh

USEREXTS="/home/kf/.local/share/SuperCollider/Extensions"

for F in $(ls faust/*.dsp);
do

faust2sc.py "$F" -s -o "$USEREXTS/kometFaustPlugins" -double -vec

done
