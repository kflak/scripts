#!/bin/bash

if [[ $# -eq 0  ]]
then 
    echo -n "filename> "
    read infile
else
    infile=$1
fi

timestamp() {
    date +"%F_%T"
}

outfile="vimeoCounts_$(timestamp)"

if [[ -f $infile ]]
then
    awk -F "," '/Sketch/ {print $8 " " $1}' $infile | sed -E 's/#([0-9]):/#0\1:/'| sort > $outfile
else
    echo "No such file: $infile"
    exit $?
fi
