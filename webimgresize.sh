#!/bin/bash

usage() {
    echo "Usage: $(basename $0) [-f filename] [-s size (default 500x333)] [-q quality (default 50%)]"
    exit 1
}

RESIZE=500x333
QUALITY=50%
while getopts 'f:s:q:h' opt; do
    case "$opt" in
        f)
            IMG="$OPTARG"
            ;;

        s)
            RESIZE="$OPTARG"
            ;;
        q)
            QUALITY="$OPTARG"
            ;;

        h|?)
            usage
            ;;
    esac
done
shift "$((OPTIND -1))"

if [ -f "$IMG" ]; then
    mogrify -resize "$RESIZE" -quality "$QUALITY" -strip "$IMG"
    identify "$IMG"
else
    echo "File name not set or not a valid path"
    usage
fi
