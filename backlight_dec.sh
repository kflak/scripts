#!/bin/bash

if [ $( cat /sys/class/backlight/gmux_backlight/actual_brightness ) -lt 20 ]
then 
    gmux_backlight 0
else
    gmux_backlight -20
fi
