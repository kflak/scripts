#!/bin/bash

BROWSER=$1
CONTENT=$2

mkdir -p /tmp/neomutt-arch
FILENAME=$(basename $CONTENT).html

cp $CONTENT /tmp/neomutt-arch/$FILENAME
$BROWSER file:///tmp/neomutt-arch/$FILENAME &
