#!/bin/bash

# xrandr --output DP-0 --auto
# xrandr --output DP-0 --mode 1920x1080
xrandr --output HDMI-A-1-0 --auto
xrandr --output HDMI-A-1-0 --right-of DP-0 --mode 1920x1080
xrandr --output eDP-1-0 --auto 
xrandr --output eDP-1-0 --below DP-0 
xrandr --output eDP-1-0 --primary
