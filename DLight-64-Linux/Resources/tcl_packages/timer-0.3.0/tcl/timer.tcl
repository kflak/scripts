# timer.tcl --

# A timer package.

# Version   : 0.4.0
# Author    : Mark G. Saye
# Email     : markgsaye@gmail.com
# Copyright : Copyright (C) 2000-2009
# Date      : November 10, 2009

# Modified  : Tania Rubio
# Email:    : yosoythania@hotmail.com
# Date:     : November, 2009

# This version has microseconds accuracy

# See the file "LICENSE.txt" or "LICENSE.html" for information on usage
# and distribution of this file, and for a DISCLAIMER OF ALL WARRANTIES.

# ======================================================================

  namespace eval timer {}

# ======================================================================

# pkginit_ --

# Description
#   Initialize this package

proc timer::pkginit_ {} {

# ----------------------------------------------------------------------

  interp alias {} [namespace current]::. {} namespace current

# ----------------------------------------------------------------------

  variable debug ; if { ![info exists debug] } { set debug 1 }

  debug 2 "proc pkginit_\n"

# ----------------------------------------------------------------------

  if { [catch {file normalize [info script]} scr] } {
    set list {}
    foreach item [file split [file join [pwd] [info script]]] {
      switch -- $item {
        "."     {}
        ".."    { set list [lreplace $list end end] }
        default { lappend list $item }
      }
    }
    set scr [eval [linsert $list 0 file join]]
    unset list
  }

  set tcl [file dirname $scr]
  set dir [file dirname $tcl]

  debug 3 "  script = \[$scr\]\n"

# ----------------------------------------------------------------------

  variable pkginfo ; array set pkginfo [list \
    package      timer \
    version      0.4.0 \
    script       $scr \
    directory    $dir \
    name        "Timer Package" \
    description "A timer package for Tcl/Tk." \
    copyright   "Copyright (C) 2000-2009" \
    author      "Mark G. Saye" \
    email        markgsaye@gmail.com  \
    date        "November 10, 2009" \
  ]

# ----------------------------------------------------------------------

  variable timer

  set timer(uid) 0

# ----------------------------------------------------------------------

  namespace export timer

# ----------------------------------------------------------------------

  set packages {
    {Tcl 8.5}
  }

  foreach package $packages {
    debug 3 "  package require \[$package\] ... \n"
    set ver [eval [linsert $package 0 package require]]
    debug 3 "  package require \[$package\] ... $ver\n"
  }

# ----------------------------------------------------------------------

  if { [string equal ::$pkginfo(package) [.]] } {
    package provide $pkginfo(package) $pkginfo(version)
  }

# ----------------------------------------------------------------------

  return

}

# ======================================================================

# cget --

# Get configuration option for timer

proc timer::cget {token option} {

  debug 2 "proc cget \[$token\] \[$option\]\n"

# ----------------------------------------------------------------------

  variable timer

  upvar #0 $token _

# ----------------------------------------------------------------------

  if { ![info exists _($option)] } {
    return -code error "unknown option \'$option\'"
  } else {
    return $_($option)
  }

# ----------------------------------------------------------------------

}

# ======================================================================

# cmd --

# Wrapper proc for timer command

proc timer::cmd_ {token command args} {

  debug 2 "proc cmd_ \[$token\] \[$command\] \[$args\]\n"

# ----------------------------------------------------------------------

  variable timer

  upvar #0 $token _

# ----------------------------------------------------------------------

  switch -- $command {
    cget -
    configure -
    delete -
    reset -
    start -
    startstop -
    stop {
      set return [uplevel [linsert $args 0 [.]::$command $token]]
    }
    default {
      return -code error "bad command \"$command\""
    }
  }

# ----------------------------------------------------------------------

  debug 3 "  return \[$return\]\n"
  return $return

}

# ======================================================================

# configure --

# Get/set configuration option(s) for timer

proc timer::configure {token args} {

  debug 2 "proc configure \[$token\] \[$args\]\n"

# ----------------------------------------------------------------------

  variable timer

  upvar #0 $token _

# ----------------------------------------------------------------------

  set argc [llength $args]

  if { $argc == 1 } {
    return [eval cget $token $args]
  } else {
    array set arg $args
  }

# ----------------------------------------------------------------------

  set switches [list -alarm -debug -interval -reset -stop -update]

  if { $argc == 0 } {
    foreach switch $switches {
      lappend list $switch $_($switch)
    }
    return $list
  } else {
    foreach switch [array names arg] {
      if { [lsearch $switches $switch] == -1 } {
        return -code error "unknown option \'$switch\'"
      } else {
        set _($switch) $arg($switch)
      }
    }
  }

# ----------------------------------------------------------------------

  return

}

# ======================================================================

# create --

# Create a new timer.

proc timer::create {args} {

  debug 2 "proc create \[$args\]\n"

# ----------------------------------------------------------------------

  variable debug
  variable timer

# ----------------------------------------------------------------------

  if { ![info exists timer(uid)] } { set timer(uid) 0 }
  set token [.]::[incr timer(uid)]
  upvar #0 $token _

# ----------------------------------------------------------------------

  array set _ [list \
    -alarm   {} \
    -debug    $debug \
    -interval 1 \
    -reset    0 \
    -stop    {} \
    -update  {} \
    running   0 \
  ]

  eval [linsert $args 0 configure $token]

# ----------------------------------------------------------------------

  reset $token

# ----------------------------------------------------------------------

  interp alias {} $token {} [.]::cmd_ $token

# ----------------------------------------------------------------------

  debug 3 "  return \[$token\]\n"
  return $token

}

# ======================================================================

# debug --

# Description
#   Debug routine to display info

proc timer::debug {level string} {

# ----------------------------------------------------------------------

  upvar token tok

  if { [info exists tok] } { upvar #0 $tok _ }

  if { [info exists _(-debug)] } {
    set debug $_(-debug)
  } else {
    variable debug
  }

# ----------------------------------------------------------------------

  set i [uplevel [list info level]]

# ----------------------------------------------------------------------

  if { $level <= $debug } {
    puts -nonewline stderr " \[$i\] timer: $string"
  }

# ----------------------------------------------------------------------

  return

}

# ======================================================================

# delete --

# Description
#   Delete a timer

proc timer::delete {token} {

  debug 2 "proc delete \[$token\]\n"

# ----------------------------------------------------------------------

  upvar #0 $token _

# ----------------------------------------------------------------------

  if { [array exists _] } {
    if { $_(running) } { stop $token }
    if { [info exists _(afterid)] } {
      catch {after cancel $_(afterid)}
    }
    array unset _
  }

# ----------------------------------------------------------------------

  return

}

# ======================================================================

# fmt --

# Description
#   Format the number of microseconds to time zero

# Arguments:
#   token : the name of the state array variable for this timer
#   t_us  : the number of microseconds relative to time zero

proc timer::fmt {token t_us} {

  debug 4 "proc fmt \[$token\] \[$t_us\]\n"

# ----------------------------------------------------------------------

  upvar #0 $token _

# ----------------------------------------------------------------------

  if { $t_us < 0 } {
    set t_us [expr {abs($t_us)}]
    set - -
  } else {
    set - ""
  }

  set t [expr {$t_us / 1000000}]
  set u [expr {$t_us % 1000000}]

  set h ${-}[expr {$t / 3600}]
  set t [expr {$t % 3600}]
  set m [expr {$t /   60}]
  set s [expr {$t %   60}]
  debug 5 "  t_us = \[$t_us\] t = \[$t\] h = \[$h\] m = \[$m\] s = \[$s\] u = \[$u\]\n"

  set _(hours)        $h
  set _(minutes)      $m
  set _(seconds)      $s
  set _(microseconds) $u

# ----------------------------------------------------------------------

  set H [format %02s $h]
  set M [format %02d $m]
  set S [format %02d $s]
  set U [format %03d $u]

  set return "$H:$M:$S"

# ----------------------------------------------------------------------

  debug 5 "  fmt return \[$return\]\n"
  return $return

}

# ======================================================================

# reset --

# Description
#   Reset the specified timer

# Arguments
#   token : the name of the timer's state array variable
#   t_us  : the time in microseconds to set the value of the timer

proc timer::reset {token {t_us ""}} {

  debug 2 "proc reset \[$token\] \[$t_us\]\n"

# ----------------------------------------------------------------------

  upvar #0 $token _

# ----------------------------------------------------------------------

  if { [string equal $t_us ""] } { set t_us $_(-reset) }

# ----------------------------------------------------------------------

  set _(now)   $t_us
  set _(time)  [fmt $token $t_us]
  set _(reset) $t_us

  set c [clock microseconds]
  set _(zero) [expr {$c - $_(now)}]

# ----------------------------------------------------------------------

  if { [string length $_(-update)] } {
    debug 3 "  uplevel #0 $_(-update) $token\n"
    uplevel #0 $_(-update) $token
  }

# ----------------------------------------------------------------------

  debug 3 "  return \[$t_us\]\n"
  return $t_us

}

# ======================================================================

# start --

# Description
#   Start the timer ticking

proc timer::start {token} {

  debug 2 "proc start \[$token\]\n"

# ----------------------------------------------------------------------

  variable timer

  upvar #0 $token _

# ----------------------------------------------------------------------

  if { $_(running) } { return }

# ----------------------------------------------------------------------

  # Get number of clicks since
  set c [clock microseconds]

  # Set the effective time at which the timer would have been started
  # i.e. synchronize the clicks with the current timer time
  set _(zero) [expr {$c - $_(now)}]
  debug 3 "  _(now) = \[$_(now)\] _(zero) =x \['$_(zero)\]\n"

# ----------------------------------------------------------------------

  set _(running) 1

  # Start the clock ticking
  tick $token

# ----------------------------------------------------------------------

  debug 3 "  start return\n"
  return

}

# ======================================================================

# startstop --

# Description
#   Toggle the running state of the timer

proc timer::startstop {token} {

  debug 2 "proc startstop \[$token\]\n"

# ----------------------------------------------------------------------

  upvar #0 $token _

# ----------------------------------------------------------------------

  if { $_(running) } { stop $token } { start $token }

# ----------------------------------------------------------------------

  return

}

# ======================================================================

# stop --

# Description
#   Stop the timer

proc timer::stop {token} {

  debug 2 "proc stop \[$token\]\n"

# ----------------------------------------------------------------------

  variable timer

  upvar #0 $token _

# ----------------------------------------------------------------------

  if { ![array exists _] } { return }

  if { !$_(running) } { return }

# ----------------------------------------------------------------------

  set _(running) 0

  if { [info exists _(afterid)] } {
    debug 3 "  afterid = \[$_(afterid)\]\n"
    debug 3 "  after info = \[[after info $_(afterid)]\]\n"
    catch {after cancel $_(afterid)}
    catch {unset _(afterid)}
  }

  tick $token

# ----------------------------------------------------------------------

  debug 3 "  stop = \[$_(-stop)\]\n"
  if { [string length $_(-stop)] } {
    debug 3 "  uplevel #0 $_(-stop)\n"
    uplevel #0 $_(-stop)
  }

# ----------------------------------------------------------------------

  return

}

# ======================================================================

# tick --

# Description
#   Main ticking routine

proc timer::tick {token} {

  debug 4 "proc tick \[$token\]\n"

# ----------------------------------------------------------------------

  upvar #0 $token _

# ----------------------------------------------------------------------

  catch {unset _(afterid)}

# ----------------------------------------------------------------------

  set last $_(now)

# ----------------------------------------------------------------------

  set _(clicks) [clock microseconds]
  set _(now) [expr {$_(clicks) - $_(zero)}]
  debug 5 "  clicks = \[$_(clicks)\] zero = \[$_(zero)\] now = \[$_(now)\]\n"

# ----------------------------------------------------------------------

  if { $_(now) < $last } {
    debug 5 "  last = \[$last\]\n"
    debug 5 "  now  = \[$_(now)\]\n"
    set large $last
    for {set x $last} {$x >= $last} {set x [expr {$x+1}]} {
      set large $x
    }
    debug 5 "  large = \[$large\] x = \[$x\]\n"
    set _(now) $large
    set _(time) [fmt $token $_(now)]

    if { [string length $_(-update)] } {
      debug 5 "  uplevel #0 $_(-update) $token\n"
      uplevel #0 $_(-update) $token
    }

    set option [tk_messageBox \
      -icon warning \
      -message "The timer has reached its maximum limit.
Would you like to reset the timer?" \
      -title "Warning" \
      -type yesno \
    ]
    switch -- $option {
      no {
      }
      yes {
        reset $token
      }
    }
    debug 5 "  set _(running) 0\n"
    set _(running) 0
    if { [string length $_(-stop)] } {
      debug 5 "  uplevel #0 $_(-stop)\n"
      uplevel #0 $_(-stop)
    }
  }

# ----------------------------------------------------------------------

  if { ($_(reset) < 0) && ($_(now) >= 0) } {
    debug 3 "  reached alarm state\n"
    set _(running) 0
    if { [string length $_(-stop)] } {
      debug 3 "  uplevel #0 $_(-stop)\n"
      uplevel #0 $_(-stop)
    }
    set _(now) 0
    set time [fmt $token $_(now)]
    debug 3 "  set _(time) \[$time\]\n"
    set _(time) $time

    if { [string length $_(-alarm)] } {
      debug 3 "  uplevel #0 $_(-alarm)\n"
      uplevel #0 $_(-alarm) $token
    }
  } else {
    set time [fmt $token $_(now)]
    debug 5 "  set _(time) \[$time\]\n"
    set _(time) $time
  }

# ----------------------------------------------------------------------

  if { [string length $_(-update)] } {
    debug 5 "  uplevel #0 $_(-update) $token\n"
    uplevel #0 $_(-update) $token
  }

# ----------------------------------------------------------------------

  debug 5 "  running = \[$_(running)\]\n"
  if { $_(running) } {
    set _(afterid) [after $_(-interval) [list [.]::tick $token]]
  }

# ----------------------------------------------------------------------

  return

}

# ======================================================================

# timer --

# Description
#   Main timer routine

proc timer::timer {args} {

  debug 2 "proc timer \[$args\]\n"

# ----------------------------------------------------------------------

  set argc [llength $args]
  set arg0 [lindex $args 0]

  if { $argc == 0 || [string match -* $arg0] } {
    set return [eval [linsert $args 0 create]]
  } else {
    set return [eval $args]
  }

# ----------------------------------------------------------------------

  debug 3 "  return \[$return\]\n"
  return $return

}

# ======================================================================

# pkgexit_ --

# Description
#   Unload this timer package

proc timer::pkgexit_ {} {

  debug 4 "proc pkgexit_\n"

# ----------------------------------------------------------------------

  variable timer

# ----------------------------------------------------------------------

  for {set i 1} {$i <= $timer(uid)} {incr i} {
    delete [.]::$i
  }

# ----------------------------------------------------------------------

  if { [catch {unset [.]::info} error] } {
    debug 0 "  \[$::errorCode\] $error\n"
  }
  if { [catch {unset [.]::timer} error] } {
    debug 0 "  \[$::errorCode\] $error\n"
  }
  if { [catch {unset [.]::debug} error] } {
    puts stderr "timer:  \[$::errorCode\] $error"
  }

# ----------------------------------------------------------------------

  foreach proc [info procs [.]::*] {
    rename $proc {}
  }

  package forget timer

  namespace delete [.]

# ----------------------------------------------------------------------

  return

}

# ======================================================================

  timer::pkginit_

# ======================================================================
