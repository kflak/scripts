#==============================================================================
# mwutil, scaleutil, and themepatch package index file.
#
# Copyright (c) 2020-2022  Csaba Nemethi (E-mail: csaba.nemethi@t-online.de)
#==============================================================================

package ifneeded mwutil     2.19 [list source [file join $dir mwutil.tcl]]
package ifneeded scaleutil  1.9  [list source [file join $dir scaleutil.tcl]]
package ifneeded themepatch 1.2  [list source [file join $dir themepatch.tcl]]
