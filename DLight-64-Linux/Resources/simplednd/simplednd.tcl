##simplednd: implements simple mechanism for drag-and-drop within Tk applications. (c) 2009 WordTech Communications LLC. License: standard Tcl license,  http://www.tcl.tk/software/tcltk/license.html.

package provide simplednd 1.1


namespace eval simplednd {


    #create the drag icon with empty text and image to initialize; then hide the icon
    proc makeDragIcon {txt img} {

	variable dragicon
	variable dragtext
	variable dragimage

	#create the icon
	set dragicon [toplevel .dnd]
	set dragtext $txt
	set dragimage $img

	wm overrideredirect $dragicon true
  

	label $dragicon.view  -image $dragimage -text $dragtext -compound left
	pack $dragicon.view

	#now hide the icon
	wm withdraw $dragicon

    }

    #register widget to respond to drag events: widget to register, its target widget, callback to associate with this drag event, text for the drag label, and image for the drag label
    proc canvasDragRegister {w mtag target dragcmd dropcmd} {

	variable dragicon
	variable dragtext
	variable dragimage
	variable targetdirection

	#catch {simplednd::makeDragIcon {} {}}

	#puts "$w registered as dragsite with $target as the drop target"

	#binding for when drag motion begins
	$w bind $mtag <Control-B1-Motion> [list [namespace current]::dragMove $w %X %Y $dragcmd $target]

	$w bind $mtag <Control-ButtonRelease-1> [list [namespace current]::dragStop $w %X %Y $target $dropcmd ]
	#binding for when drop event occurs
	


    }

    proc dragRegister {w target dragcmd dropcmd} {

	variable dragicon
	variable dragtext
	variable dragimage
	variable targetdirection

	#catch {simplednd::makeDragIcon {} {}}

	#puts "$w registered as dragsite with $target as the drop target"

	#binding for when drag motion begins
	bind $w <B1-Motion> [list [namespace current]::dragMove %W %X %Y $dragcmd $target]

	bind $w <ButtonRelease-1> [list [namespace current]::dragStop %W %X %Y $target $dropcmd ]
	#binding for when drop event occurs
	


    }

    #drag motion with following args: source widget, cursor x position, cursor y position, drag command, target widget
    proc dragMove {w x y dragcmd target} {

	variable dragicon
	variable dragtext
	variable dragimage
	variable targetdirection

	#the dragcmd properly configures the drag icon
	eval $dragcmd

	
	[namespace current]::trackCursor $w $x $y $target 

    }


    #track the cursor, change if it is over the drop target; args are source widget (w), x pos (x), y pos (y), target widget (target)
    proc trackCursor {w x y target} {
	
	#get the coordinates of the drop target
	set targetx [winfo rootx $target]
	set targety [winfo rooty $target]
	set targetwidth [expr [winfo width $target] + $targetx]
	set targetheight [expr [winfo height $target] + $targety]

	
	#change the icon if over the drop target
	if {($x > $targetx) && ($x < $targetwidth) && ($y > $targety) && ($y < $targetheight)} {
	    $w configure -cursor based_arrow_down
	} else {
	    $w configure -cursor dot
	}   
    }



   #dragstop/drop event with following args:  source widget, cursor x position, cursor y position, target widget, dropcommand: if over drop target, execute dropcommand; otherwise simply return
   proc dragStop {w x y target dropcmd} {
       set done 0
       variable dragicon
       variable dragtext
       variable dragimage
       variable targetdirection
       
       #hide dragicon on drop event
       #wm withdraw $dragicon
       
       #change cursor back to arrow
       $w configure -cursor ""
       
       #execute callback or simply return
       if {[winfo containing $x $y] eq $target} {
           set done $dropcmd
       } elseif {[winfo containing $x $y] ne {.main3.c}} {
           foreach z [winfo children $target] {
               if {[winfo parent [winfo containing $x $y]] eq $z
                   || [winfo parent [winfo parent [winfo containing $x $y]]] eq $z
                   || [winfo parent [winfo parent [winfo parent [winfo containing $x $y]]]] eq $z
                   || [winfo parent [winfo parent [winfo parent [winfo parent [winfo containing $x $y]]]]] eq $z} {
                       #eval $dropcmd $z $x $y
                       set done [list $dropcmd $z $x $y]
                       break;
                   }
           }
       }
       
       if {$done eq 0} {
           ::dnd::fakedrop [winfo containing $x $y] $x $y
           #puts "target $target not reached"
       } else {
           #puts "target $target Reached"
           focus -force $target
           eval $done  
       }
   }


    #demo package
    proc demo {} {
	
	variable dragicon
	variable dragtext
	variable dragimage


	#create image for demo
	image create photo dnd_demo -data {R0lGODlhEAAQALMAAAAAAMbGxv//////////////////////////////////\
					  /////////////////////yH5BAEAAAEALAAAAAAQABAAAAQwMMhJ6wQ4YyuB\
					  +OBmeeDnAWNpZhWpmu0bxrKAUu57X7VNy7tOLxjIqYiapIjDbDYjADs=}

	listbox .l -selectmode single -activestyle none
	listbox .b -selectmode single -activestyle none

	foreach item {do re mi} {
	    .l insert end $item
	}

	foreach item {fa so la} {
	    .b insert end $item
	}
	
	pack .l -side left
	pack .b -side right
	

	#register drag sources, drag targets, and callbacks
	dragRegister .l .b [namespace current]::drag_l [namespace current]::drop_l

	dragRegister .b .l [namespace current]::drag_b [namespace current]::drop_b

    }

    #dragcommand for demo l widget: configures dragicon
    proc drag_l {} {

	variable dragicon
	variable dragtext
	variable dragimage

	set item [lindex [.l get [.l curselection]]]
	set dragtext $item
	set dragimage dnd_demo

    }


    #dropcommand for demo l widget: callback to execute on drop
    proc drop_l {} {

	variable dragicon
	variable dragtext
	variable dragimage

	.b insert end $dragtext
	
	.l delete [.l curselection]
    }

    #dragcommand for demo b widget: configures dragicon
    proc drag_b {} {

	variable dragicon
	variable dragtext
	variable dragimage

	set item [lindex [.b get [.b curselection]]]
	set dragtext $item
	set dragimage dnd_demo

    }


    #dropcommand for demo b widget: callback to execute on drop
    proc drop_b {} {

	variable dragicon
	variable dragtext
	variable dragimage

	.l insert end $dragtext
	
	.b delete [.b curselection]
    }



    namespace export *

}
