#!/bin/sh
 cd `dirname $0`
my_path=`pwd`

/sbin/ldconfig -n $my_path/bin/
export PATH=$my_path/bin/:$PATH
export LD_LIBRARY_PATH=$my_path/bin/:/usr/local/lib/:$LD_LIBRARY_PATH
chmod +x $my_path/bin/DLight
$my_path/bin/DLight $1
