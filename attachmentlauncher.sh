#!/bin/bash

application=$1
file=$2
tempdir=~/.tmp/neomutt-arch
newfile=$tempdir/$(basename "$file")

if [ -d $tempdir ]; then
    rm -f $tempdir/*
else
    mkdir -p $tempdir
fi

cp "$file" "$newfile"

$application "$newfile"
