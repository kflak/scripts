#!/bin/bash

filename=$(date +%F).md
cd ~/Documents/worksheets
if [[ ! -f filename ]]; then
    cp template.md $filename
fi
nvim $filename
gpg -c $filename
