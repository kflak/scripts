#!/bin/bash
folder=$1
# extension='_mono.wav'

if [ -d $folder ]; then
    for filename in $folder/*; do
        if [ -f $filename ] ; then
            sox $filename "tmp" channels 1
            mv tmp $filename
            echo $filename
        fi
    done
fi
