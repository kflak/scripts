#!/bin/bash

file=$1
filename=$(basename "$file")
ext="${filename##*.}"
tmpfile=$(mktemp).$ext

if [[ -f "$file" ]]; then
    ffmpeg -i "$file" -c copy -an $tmpfile
    mv $tmpfile "$file"
fi
