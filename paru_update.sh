#!/bin/bash

PKGNAME=paru
FOLDER=/tmp

questionnaire() {
    read -e -p "
    $1? [Y/n] " YN
    [[ $YN == "y" || $YN == "Y" || $YN == "" ]] && cd $FOLDER/$PKGNAME && $2
    [[ $YN == "n" ]] && echo Abort
}

addon() {
    if ! command -v lsparu &> /dev/null
    then
        questionnaire "Install lsparu" "paru -S lsparu"
    fi
}

makeit() {
    questionnaire "Build and install $PKGNAME" "makepkg -si"
    addon
}

if [ ! -d "$FOLDER/$PKGNAME" ] 
then
    printf 'Clone PKGBUILD? (y/n)'
    old_stty_cfg=$(stty -g)
    stty raw -echo
    answer=$( while ! head -c 1 | grep -i '[ny]' ;do true ;done )
    stty $old_stty_cfg
    if echo "$answer" | grep -iq "^y" ;then
        cd /tmp
        git clone https://aur.archlinux.org/$PKGNAME.git
        makeit
        exit
    else
        echo Abort
        exit
    fi
    echo
else
    makeit
    exit
fi
