#!/bin/zsh

infile=$1
infile_basename=${infile:r}
extension=${infile:e}

if [[ extension=~"[wav|aiff|snd]" && $(soxi -c $infile) -eq 2 ]]; then
    sox $infile "$infile_basename"_left."$extension" remix 1
    sox $infile "$infile_basename"_right."$extension" remix 2
else
    echo "Not a stereo file!"
fi
