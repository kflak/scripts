#!/bin/bash

url=$1
password=$2

mpv --ytdl-raw-options=video-password=$password $url

exit
