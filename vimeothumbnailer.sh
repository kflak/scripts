#!/bin/bash

page="$1" 
thumbnaildir="$2"

if [[ -f "$page" ]]; then
    id=$(grep -n ^vimeo = $page | awk '{print $3}')

    thumbnaildir=~/html/roosnaflak.gitlab.io/static/images/sketchThumbs

    echo "Using thumbnail directory: $thumbnaildir" 

    fullpath=$(readlink -f $page)
    file=$(basename -- "$page")
    filename="$thumbnaildir/${file%.*}.jpg" 

    url=$(curl http://vimeo.com/api/v2/video/$id.json | \
        jq '.[0].thumbnail_large' | \
        sed s/\"//g\
    )

    echo $url
    echo "Downloading thumbnail"

    curl $url -o $filename

    echo "Compressing filesize"
    if [[ $? -eq 0 ]]; then
        mogrify -resize 500x333 -quality 50% -strip $filename
        exit 0
    else
        exit 1
    fi
else
    echo "Please specify page!"
    exit 1
fi
