#!/bin/bash

infile=$1
basename=$(echo "$infile" | cut -f 1 -d '.')
outfile=$basename.epub
pandoc -o $outfile $infile
ebook-viewer $outfile &
