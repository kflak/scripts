#!/bin/bash

case "$1" in 
    "rme")
        device=UCX$(lsusb | grep Fireface | awk 'NF>1{print $NF}' | sed -e 's/[()]//g'),0
        nperiods=3
        inchannels=18
        outchannels=18
        period=512
        soundcard=0
        rate=48000
        ;;
    "internal")
        case $(hostname) in
            "a15")
                device=Generic_1,0 
                ;;
            "t480s")
                device=PCH,0
                ;;
        esac
        nperiods=2
        inchannels=2
        outchannels=2
        period=1024
        soundcard=0
        rate=48000
        ;;
    "hdmi")
        case $(hostname) in
            "a15")
                device=Generic_1,0 
                ;;
            "t480s")
                device=PCH,7
                ;;
        esac
        nperiods=2
        inchannels=2
        outchannels=2
        period=512
        soundcard=0
        rate=48000
        ;;
    *)
        notify-send -t 3000 "No soundcard specified"
        soundcard=1
        ;;
esac

if [ $soundcard -eq 0 ]
then
    jack_control stop
    jack_control ds alsa
    jack_control dps device hw:$device
    jack_control dps capture hw:$device
    jack_control dps playback hw:$device
    jack_control dps rate $rate
    jack_control dps nperiods $nperiods
    jack_control dps inchannels $inchannels
    jack_control dps outchannels $outchannels
    jack_control dps period $period
    jack_control dps duplex true
    jack_control dps midi-driver seq
    jack_control start

    wait $!

    jack_control status > /dev/null
    status=$? 
    if [ $status -eq 0 ]
    then
        case $rate in  
            44100) sr="44.1k" ;;
            48000) sr="48k" ;;
            96000) sr="96k" ;;
            *) sr="-" ;;
        esac
        notify-send -t 3000 "$device connected"
        echo $device $period/$sr > $HOME/.local/share/jackinfo
    else
        notify-send -t 3000 "Error: Jack didn't start"
        echo "Jack didn't start" > $HOME/.local/share/jackinfo
    fi
fi
