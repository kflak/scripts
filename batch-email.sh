#!/bin/bash

file=$1
recipient=$2

if [[ -f $file && $recipient ]]; then
    echo $file $recipient
    /usr/bin/neomutt -s "$file" -a "$file" -B -d 2 -- $recipient
fi
