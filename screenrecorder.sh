#!/bin/bash

dir=$HOME/screenrecordings

if [[ ! -d $dir ]]; then
    mkdir $dir
fi

file=$dir/$(date +%F_%T).mp4
output=$(hyprctl -j monitors | jq -r '.[] | select(.focused)' | jq -r '.name')
# output=$(swaymsg -t get_outputs | jq -r '.[] | select(.focused)' | jq -r '.name')

if pgrep -x wf-recorder >/dev/null 
then
    kill -2 $(pgrep wf-recorder)
    notify-send -t 1000 "Finished recording" 
else 
    notify-send -t 1000 "Recording to $file" 
    # wf-recorder -c h264_vaapi -d /dev/dri/renderD128 -a -d="alsa_output.usb-RME_Fireface_UCX__23815246__F9C767BDDD2EEC8-00.pro-output-0" -o $output -f $file 
    wf-recorder "$@" -o $output -f $file

fi 
