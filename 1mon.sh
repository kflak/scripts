#!/bin/bash

xrandr --output DP-0 --off
xrandr --output HDMI-A-2 --off
xrandr --output eDP-1 --auto --primary
killall polybar
bspc monitor eDP-1 -d 一 二 三 四 五 六 七 八 九 十
polybar internal &
polybar external &
sleep 1
bspc wm -O eDP-1
